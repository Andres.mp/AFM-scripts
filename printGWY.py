# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 11:00:14 2016

@author: amunizp
@licence: gpl v3 or later and/or gfdl v1.2 or later

PrintGwy.py

A short guidance on how to 3D print your AFM/SPM data should work cross 
platform. Prerequirements:

    Install Gwyddion from http://gwyddion.net/
    Install openscad from http://www.openscad.org/
    Install python from python foundation, python(x,y) or anaconda. 
    This guide is in python3 but should work with minor or no modifications in 
    python2.7
    In openscad work out these examples to get you going: 
    https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Importing_Geometry#Text_file_format

Considerations:

Normally you can export SPM data using the gwyddion as ASCII txt file with 
1 precision, no need for decimal places and without header

Note, all units in metres by gwyddion by default. But when they pass through 
openSCAD all units are thrown away.

if too many pixels see later issue reduce the size by gwyddion data process> basic operation
> scale... 
"""
import numpy as np


def convertToDat (FileName):
    data = np.genfromtxt(FileName,delimiter="\t")
    PEAKtoPEAK = data.ptp()
    PRINTEDRANGE =  100 
    print (PEAKtoPEAK)
    '''
    in my case 100 was too little. Better scale it here than have to scale it
    in your slicer. See bellow.
    '''
    conversion= PRINTEDRANGE/PEAKtoPEAK
    dataForOPENSCAD = data*conversion
    dataForOPENSCAD = np.around(dataForOPENSCAD, decimals=0)
    dataForOPENSCAD = dataForOPENSCAD.astype(int)
    #print (dataForOPENSCAD)
    return dataForOPENSCAD

FileNames = ["2014 09 24_1b-LowRes.txt"
             ]
            
print (FileNames)

for file in FileNames:
    OutFileName = file+".dat"
    
    scadFile = ("""//printGWY.scad
scale([0.5,0.5,1]) surface(file = "{0}", center = true, convexity = 5);
// draw a  50 nm tall block
translate([38,-64,0]) cube([25,25,7.16],centre=true);


""".format(OutFileName))
    with open(file+".scad", 'w' ) as f:
        f.write(scadFile)
    dataForOpenSCAD = convertToDat(file)
    np.savetxt(OutFileName,dataForOpenSCAD)
        
'''
Take the $OutFileName and run it in openscad with the following code extracted 
directly from the example mentioned above. Unsure what the commands do. 


########
//printGWY.scad
surface(file = "opensCADdata.dat", center = true, convexity = 5);
%translate([0,0,5])cube([10,10,10], center =true);
########
Once the 3D view is of your liking, render and create STL model.

The STL file might have some problems:
a) Might be that the file is too large. This might be sorted by changing the 
numbers to integers. 
b) the STL file might be a surface. Somehow our slicer (CURA) seemed to print
anyway.
c) creating the STL file seems to be computer intensive.

After that insert the stl model in your favourite slicer such as CURA. There
you can scale it to fit your printing table surface or exagerate the z scale. 
Finally you would have generated your gcode (especific for each printer). 
that gcode goes to the printer.


TODO:     * find out if Gwyddion 3D view can be saved as an object file(OBJ): 
          some slicers such as CURA can open files in that format (instead of
          stl)
          * Change numbers to integers to see if STL file is smaller 
          DONE, does not seem to help
          * Check if python script can be replaced by a gwy Save As... module
          output would be *.dat file and openSCAD file. 
          * Test it with other printers
          * check if STL surface causes issues.
          * Find out how to create a thicker layer under data, this will
          help remove from under the plate
          * is it worth looking into SolidPython? would save another conversion 
          https://pypi.python.org/pypi/solidpython
'''

#sdhfas